import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JPanel root;
    private JButton ramenButton;
    private JButton sobaButton;
    private JButton udonButton;
    private JTextPane textbox;
    private JButton Friedrice;
    private JButton tempura;
    private JButton Katudon;

    private JTextPane total;
    private JButton shop;
    private JLabel Ramen;
    private JLabel soba;
    private JLabel tyahan;
    private JLabel Tempura;
    private JLabel katudon;

    static int Sum = 0;
    static int set1 = 0;
    static int set2 = 0;
    static int set3 = 0;

    public FoodGUI() {
        ramenButton.setIcon(new ImageIcon(
                this.getClass().getResource("ラーメン.jpg")
        ));
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String food ="Ramen";
                int fee = 750;
                order(food,fee);
            }
        });

        sobaButton.setIcon(new ImageIcon(
                this.getClass().getResource("そば.jpg")
        ));
        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String food = "Soba";
                int fee = 700;
                order(food,fee);
            }
        });
        udonButton.setIcon(new ImageIcon(
                this.getClass().getResource("udonn.jpg")
        ));
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String food = "Udon";
                int fee = 680;
                order(food,fee);
            }
        });
        shop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                check();
            }
        });


        Katudon.setIcon(new ImageIcon(
                this.getClass().getResource("かつどん.jpg")
        ));
        Katudon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String food = "Katudon";
                int fee = 600;
                order(food,fee);
            }
        });
        tempura.setIcon(new ImageIcon(
                this.getClass().getResource("天ぷら.jpg")
        ));
        tempura.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String food = "tempura";
                int fee = 300;
                order(food,fee);
            }
        });
        Friedrice.setIcon(new ImageIcon(
                this.getClass().getResource("ちゃーはん.jpg")
        ));
        Friedrice.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String food = "Fried rice";
                int fee = 500;
                order(food,fee);
            }
        });
    }
    void order(String food,int fee){
        int confirmation1 = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " +food+ "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if (confirmation1 == 0) {
            if(food == "Udon" || food == "Ramen" || food == "Soba"){
                int confirmation2 = JOptionPane.showConfirmDialog(
                    null,
                    "Would you like a large portion for 50 yen?",
                    "big Confirmation",
                    JOptionPane.YES_NO_OPTION
                 );
                if (food == "Ramen") {
                    FoodGUI.set1 = 1;
                }
                if (food == "Soba") {
                    FoodGUI.set2 = 1;
                }
                if (food == "Udon") {
                    FoodGUI.set3 = 1;
                }
                if(confirmation2 == 0){
                    food += "(large)";
                    fee += 50;
                }
            }
            if(food == "Fried rice" && FoodGUI.set1==1){
                fee-=150;
                food += "(set)";
            }
            if(food == "Katudon" && FoodGUI.set2 == 1){
                fee-=150;
                food += "(set)";
            }if(food == "tempura" && FoodGUI.set3==1){
                fee-=150;
                food += "(set)";
            }
            FoodGUI.Sum += fee;
            String currentText = textbox.getText();
            textbox.setText(currentText +food+"   "+ fee+"yen\n");
            total.setText("Total    "+ FoodGUI.Sum +"yen");
             JOptionPane.showMessageDialog (
                     null,
                     "Thank you for Ordering for " +food+ "! it will be served as soon as possible." );
        }
    }

    void check(){
        int pay = JOptionPane.showConfirmDialog(
                null,
                "Thank you! The totl price is " + FoodGUI.Sum +"yen",
                "pay Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(pay == 0) {
            FoodGUI.set1 = 0;
            FoodGUI.set2 = 0;
            FoodGUI.set3 = 0;
            FoodGUI.Sum = 0;
            String currentText = textbox.getText();
            textbox.setText("");
            String currentText2 = total.getText();
            total.setText("Total    "+ FoodGUI.Sum +"yen");

        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
